package nl.codenomads.batm.web

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class AtmController {
    @GetMapping
    fun testMethod() = "Hello BATM"
}
