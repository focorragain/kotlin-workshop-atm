package nl.codenomads.batm

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class BatmApplication

fun main(args: Array<String>) {
	runApplication<BatmApplication>(*args)
}
