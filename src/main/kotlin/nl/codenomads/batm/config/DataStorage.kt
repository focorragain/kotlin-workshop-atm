package nl.codenomads.batm.config

import nl.codenomads.batm.model.Balance
import nl.codenomads.batm.model.User
import org.springframework.stereotype.Component
import java.math.BigDecimal
import java.util.*
import javax.annotation.PostConstruct

@Component
open class DataStorage {

    private var data: MutableMap<String, User> = mutableMapOf()

    @PostConstruct
    fun init() {
        data["Ferdia"] = User("Ferdia", "0000", listOf(
                Balance(Currency.getInstance("EUR"), BigDecimal(50000)),
                Balance(Currency.getInstance("USD"), BigDecimal(8000))
        ))
        data["Carmen"] = User("Carmen", "1111", listOf(
                Balance(Currency.getInstance("EUR"), BigDecimal(80000))
        ))
        data["Alexander"] = User("Alexander", "2222", listOf(
                Balance(Currency.getInstance("EUR"), BigDecimal(30000)),
                Balance(Currency.getInstance("USD"), BigDecimal(7000)),
                Balance(Currency.getInstance("RUB"), BigDecimal(700000))
        ))
        data["Ramon"] = User("Ramon", "9876", listOf(
                Balance(Currency.getInstance("EUR"), BigDecimal(300000)),
                Balance(Currency.getInstance("USD"), BigDecimal(70000))
        ))
    }
}