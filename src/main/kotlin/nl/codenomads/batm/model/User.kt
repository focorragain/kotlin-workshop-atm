package nl.codenomads.batm.model

data class User(val name: String, val password: String, val balances: List<Balance>)