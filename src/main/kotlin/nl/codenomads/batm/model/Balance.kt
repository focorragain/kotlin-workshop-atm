package nl.codenomads.batm.model

import java.math.BigDecimal
import java.util.*

data class Balance(val currency: Currency, val amount: BigDecimal)